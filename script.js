// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд.
//У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const iconWithFlash = document.querySelectorAll(".fa-eye-slash");
const iconWithoutFlash = document.querySelectorAll(".fa-eye");

iconWithoutFlash.forEach((element) => {
  element.addEventListener("click", () => {
    const target = element.getAttribute("data-icon");
    let inputPassword = document.querySelector(target);
    if (inputPassword.type === "password") {
      inputPassword.type = "text";
      element.classList.replace("fa-eye", "fa-eye-slash");
    }else{
      inputPassword.type = "password";
      element.classList.replace("fa-eye-slash", "fa-eye")
    }
  });
});

const btn = document.querySelector("button");

btn.addEventListener("click", (ev) => {
  const enterPassword = document.querySelector("#enter-password");
  const confirmPassword = document.querySelector("#confirm-password");

  ev.preventDefault();

  if (enterPassword.value != confirmPassword.value) {
    const span = document.createElement("span");
    btn.before(span);
    span.classList.add("span");
    span.textContent = "Потрібно ввести однакові значення";

  } else {
    alert("You are welcome");
  }
});
